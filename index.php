<?php

require "vendor/autoload.php";
require "src/functions.php";

use Michelf\Markdown;



$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addGlobal('ma_valeur', "Hello There!");
    $twig->addFilter(new Twig_Filter('trad', function($string){
        return $string;
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});


Flight::route('/livres/', function(){
    $data = [
        'livres' => getBooks(),
        
    ];
    
});

Flight::route('/', function(){

    $data = [
        
        'dinosaurs' => getAllDinos(),
        
    ];

    Flight::render('dino_list.twig',$data);
});



Flight::route('/dinosaur/@dino_name/', function($dino_name){
    $data = [
        'dino' => getDinos($dino_name),
        'dinosaurs' => getAllDinos(),
        
    ];

    Flight::render('dino_dino.twig', $data);

});

Flight::route('/markdown_test', function(){

    $filepath='pages\test.md';

    $hello=readFileContent($filepath);

    $data = [
        'hello' =>renderHTMLFromMarkdown($hello),
        
    ];

    Flight::render('markdown.twig', $data);
});

Flight::route('/@name', function($name){
    echo "Pipou sur $name";
});





Flight::start();



