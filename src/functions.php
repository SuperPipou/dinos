<?php
use Michelf\Markdown;

function getDinos($dino_name)
{
    $uri = "https://allosaurus.delahayeyourself.info/api/dinosaurs/$dino_name";
    $response = Requests::get($uri);
    $dino = json_decode($response->body);
    return $dino;
}

function getAllDinos()
{
    $uri = "https://allosaurus.delahayeyourself.info/api/dinosaurs/";
    $response = Requests::get($uri);
    $alldinos = json_decode($response->body);
    return $alldinos;
}

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}

function readFileContent($filepath){
    return file_get_contents($filepath);
}


function randomDino($dino_count)
    {
        $dino_random = array("Dilophosaurus","Velociraptor","Brachiosaurus","Parasaurolophus","Triceratops","Tyrannosaurus ","Gallimimus","Gallimimus");
        $random_keys = array_rand($dino_random, 8); // On récupére trois clés aléatoires
        echo $dino_random[$random_keys[0]];
        echo $dino_random[$random_keys[1]];
        echo $dino_random[$random_keys[2]];
        echo $dino_random[$random_keys[3]];
        echo $dino_random[$random_keys[4]];
        echo $dino_random[$random_keys[5]];
        echo $dino_random[$random_keys[6]];
        echo $dino_random[$random_keys[7]];

    }